import sys
import yaml
import json
filename_yaml = str(sys.argv[1]) + "/" + str(sys.argv[2]) + ".yaml" 
filename_json = str(sys.argv[1]) + "/" + str(sys.argv[2] )
os_list = {}
with open(filename_yaml) as infile:
     os_list = yaml.load(infile, Loader=yaml.FullLoader) 
with open(filename_json, 'w') as outfile:
     json.dump(os_list, outfile, indent=4, sort_keys=True) 
     print(filename_json + " file written.")
