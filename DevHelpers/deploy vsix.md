
Procédure pour déployer l'extension VSIX et vscode MarketPlace
====
 
 ! à faire sur la branche 'master' alors que tout est commité

L'extension estdéployée via l'outil VSCE 
c.f. https://code.visualstudio.com/api/working-with-extensions/publishing-extension 


# install node.js
Le système node est nécessaire
c.f. https://nodejs.org/en/download/ 

# install vsce 
vsce, short for "Visual Studio Code Extensions", is a command-line tool for packaging, publishing and managing VS Code extensions.

Executer au shell : `npm install -g vsce`

# Personal Acces Token 
Vous avz besoin de définir vos accès identifié via Microsoft Azure DevOps.
Notre organisation  : https://dev.azure.com/infodatagroup/

c.f. https://code.visualstudio.com/api/working-with-extensions/publishing-extension#get-a-personal-access-token 

# Editer le .vsixManifest 
`code .vsixmanifest`

# Créer un package au format vsix 
Executer au shell : `vsce package -o ./.vsix` en considérant que le répertoire .vsix existe 

# Publier le package vers VSCode MarketPlace

Executer au shell : `vsce publish patch -m 'commentaire' ` 



