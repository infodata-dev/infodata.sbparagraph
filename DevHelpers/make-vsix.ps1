# SCRIPT POUR CRER UN PACKAGE ++
$c = read-host "commentaire de patch"
vsce publish patch -m "$c"

$package = get-content .\package.json | convertfrom-json 
Write-Host "Version courante : $($package.version)"
$f = ".\.annexeEfv\.vsix\sbparagraph-$($package.version).vsix"
vsce package --out .\.annexeEfv\.vsix 
copy-item $f \\winprod\dev\Logiciels_install\infodata.sbparagraph.vscode-extension\ -verbose -force
copy-item $f \\winprod\dev\Logiciels_install\infodata.sbparagraph.vscode-extension\sbparagraph-last.vsix -verbose -force

