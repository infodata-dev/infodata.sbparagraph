import sys
import yaml
import json
filename_yaml = str(sys.argv[1]) + "/" + str(sys.argv[2]) + ".yaml" 
filename_json = str(sys.argv[1]) + "/" + str(sys.argv[2] )
os_list = {}
with open(filename_json) as infile:
     os_list = json.load(infile) 
with open(filename_yaml, 'w') as outfile:
     yaml.dump(os_list, outfile, indent=4) 
     print(filename_yaml + " file written.")
