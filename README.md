# README #

### What is this repository for? ###

Cette extension se veut simpliste.
infodata.sbparagraph défini un contributes.languages 'sbparagraph' meta language de PRocess PD.P sous SB+/SBXA

c.f. https://wiki.infodata.lu/devtools/vscode/vsc4sbparagraph 
c.f. https://bitbucket.org/infodata-dev/infodata.sbparagraph/src/master/

### Contenu
## Language configuration
- Define sbParagraph as language code (directory **\*PROCESS.VSC\* or file .sbp, .pdp)

## Snippets
- sbParagraph := all Statement of SB Pd.P and SB Expression
- sbSubroutines := all SB Subr as docuemnted by SB himslef 
- TUBP : all used TUBP subroutines 

### Contact 
* Repo owner or admin : efv@infodata.lu
* Other community or team contact : jcd@infodata.lu 

