# Infodata - IN Tools Extension change Log.

* 0.1.0 : creation
* 0.1.0 16/03/2022 contributes.languages.sbparagraph : TMlanguage.json == colorisation syntaxique
* 0.2.0 18/03/2022 contributes.snippets.sbparagraph : all SBP statement and function / expression elements 
* 0.2.1 21/03/2022 contributes.keybindings F2 -> GC PRocess 
* 0.3.0 23/03/2022 contributes.snippets.sbsubroutines > all standard SB.subr 
* 0.3.2 23/03/2022 contributes.snippets.sbsubroutines > add SB.REMOTE.PROCESS 
* 0.4.0 06/04/2022 contributes.snippets.TUBP 
* 0.5.0 : update to use vscode marketplace 
* 0.5.6 13/06/2022 contributes.configuration userSetting (credentials) workspaceSettings (server/OS/OE/account/SysId)
* 0.5.8 23/06/2022 contributes.problemPatterns sbpProblemPatternGpVscGcRmtPDP 
* 0.5.8 23/06/2022 contributes.problemMaters sbpGpVscGcRmtProblemMatcher 
* 0.5.9 01/07/2022 ajoute le répertoire Tasks avec les fichiers Tasks.json par defaut pour nos plateformes UVssh, UvLocal, D3Ssh
* 0.5.10 04/07/2022 ajoute les variables du common /SBPLUS/ 
